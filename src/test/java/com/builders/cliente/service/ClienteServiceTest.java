package com.builders.cliente.service;

import com.builders.cliente.entities.Cliente;
import com.builders.cliente.exception.BusinessException;
import com.builders.cliente.repositories.ClienteRepository;
import com.builders.cliente.utils.Constants;
import com.builders.cliente.vo.ClienteVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {ClienteServiceTest.class})
public class ClienteServiceTest {

    private ClienteService clienteService;
    private ClienteRepository clienteRepository;
    private Cliente cliente;

    @BeforeEach
    void setUp() {
        clienteRepository = mock(ClienteRepository.class);
        clienteService = new ClienteService(clienteRepository);

        cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNome("André Caixeta");
        cliente.setCpf("99999999999");
        cliente.setDataNascimento(getDataNascimento("13/07/1985"));
    }

    private Calendar getDataNascimento(String data) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DD_MM_YYYY);
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(data));
            return cal;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void shouldSaveNewCliente() {
        ClienteVO clienteVO = new ClienteVO();
        clienteVO.setNome("André Caixeta");
        clienteVO.setCpf("99999999999");
        clienteVO.setDataNascimento(getDataNascimento("31/12/1980"));

        when(clienteRepository.save(cliente)).thenReturn(cliente);
        clienteService.saveClient(clienteVO);
        verify(clienteRepository).save(any(Cliente.class));
    }

    @Test
    public void shouldFailWhenSaveClientEmptyCpf() {
        ClienteVO clienteVO = new ClienteVO();
        clienteVO.setNome("André Caixeta");
        clienteVO.setCpf("");

        when(clienteRepository.save(cliente)).thenReturn(cliente);
        Exception exception = assertThrows(BusinessException.class, () -> {
            clienteService.saveClient(clienteVO);
        });
        assertTrue(exception.getMessage().contains(Constants.CAMPOS_OBRIGATORIOS_N_INFORMADOS));
        verify(clienteRepository, never()).save(any(Cliente.class));
    }

    @Test
    public void shouldFailWhenSaveClientEmptyDataNascimento() {
        ClienteVO clienteVO = new ClienteVO();
        clienteVO.setNome("André Caixeta");
        clienteVO.setCpf("00337890913");
        clienteVO.setDataNascimento(null);

        when(clienteRepository.save(cliente)).thenReturn(cliente);
        Exception exception = assertThrows(BusinessException.class, () -> {
            clienteService.saveClient(clienteVO);
        });
        assertTrue(exception.getMessage().contains(Constants.CAMPOS_OBRIGATORIOS_N_INFORMADOS));
        verify(clienteRepository, never()).save(any(Cliente.class));
    }
}