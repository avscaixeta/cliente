CREATE TABLE "public"."cliente" (
	id              int8 NOT NULL,
	nome            varchar(100) NOT NULL,
	cpf             varchar(11) NOT NULL,
	data_nascimento date NOT NULL,
	created_at      timestamp NOT NULL,
	CONSTRAINT cliente_pkey PRIMARY KEY (id),
	CONSTRAINT cliente_un   UNIQUE (cpf)
);

CREATE SEQUENCE "public".seq_cliente;