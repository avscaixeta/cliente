package com.builders.cliente.service;

import com.builders.cliente.entities.Cliente;
import com.builders.cliente.exception.BusinessException;
import com.builders.cliente.mappers.ClienteMapper;
import com.builders.cliente.repositories.ClienteRepository;
import com.builders.cliente.utils.Constants;
import com.builders.cliente.vo.ClienteVO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class ClienteService {

    private final ClienteRepository clienteRepository;

    public Page<ClienteVO> findPaginatedClients(String nome, String cpf, int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return clienteRepository.findClientesByNomeStartingWithAndCpfStartingWith(nome, cpf, pageable)
                .map(ClienteMapper.INST::toClienteVO);
    }

    @Transactional
    public void saveClient(ClienteVO clienteVO) {
        checkRequiredFields(clienteVO);
        Cliente cliente = ClienteMapper.INST.toEntity(clienteVO);
        clienteRepository.save(cliente);
    }

    @Transactional
    public void updateClientName(String cpf, String nomeNovo) {
        Cliente cliente = findClienteByCpf(cpf);
        cliente.setNome(nomeNovo);
        clienteRepository.save(cliente);
    }

    @Transactional
    public void updateClient(ClienteVO clienteVO) {
        Cliente cliente = findClienteByCpf(clienteVO.getCpf());
        cliente.setNome(clienteVO.getNome());
        cliente.setDataNascimento(clienteVO.getDataNascimento());
        clienteRepository.save(cliente);
    }

    @Transactional
    public void deleteClient(String cpf) {
        Cliente cliente = findClienteByCpf(cpf);
        clienteRepository.delete(cliente);
    }

    private Cliente findClienteByCpf(String cpf) {
        return clienteRepository.findByCpf(cpf).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, Constants.NENHUM_CLIENTE_ENCONTRADO_COM_ESSE_CPF));

    }

    private void checkRequiredFields(ClienteVO clienteVO) {
        if (clienteVO.getNome().isEmpty() || clienteVO.getCpf().isEmpty() || clienteVO.getDataNascimento() == null) {
            throw new BusinessException(Constants.CAMPOS_OBRIGATORIOS_N_INFORMADOS);
        }
    }
}

