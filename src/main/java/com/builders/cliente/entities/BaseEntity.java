package com.builders.cliente.entities;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Calendar;

@Getter
@MappedSuperclass
public abstract class BaseEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdAt", nullable = false)
    private Calendar createdAt;

    public BaseEntity() {
        super();
    }

    @PrePersist
    protected final void updateTimeInfoBeforePersist() {
        this.createdAt = Calendar.getInstance();
    }

}
