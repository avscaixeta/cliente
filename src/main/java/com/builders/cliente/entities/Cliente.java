package com.builders.cliente.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Calendar;

@Data
@Entity
@Table(name = "cliente")
public class Cliente extends BaseEntity{

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="SEQ_CLIENTE")
    @SequenceGenerator(allocationSize = 1, name="SEQ_CLIENTE", sequenceName="SEQ_CLIENTE")
    @Column(name = "id")
    private Long id;

    @Column(unique = true)
    private String cpf;

    @Column
    private String nome;

    @Temporal(TemporalType.DATE)
    @Column
    private Calendar dataNascimento;
}

