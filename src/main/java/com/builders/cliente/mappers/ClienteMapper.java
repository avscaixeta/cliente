package com.builders.cliente.mappers;

import com.builders.cliente.entities.Cliente;
import com.builders.cliente.vo.ClienteVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ClienteMapper {

    ClienteMapper INST = Mappers.getMapper(ClienteMapper.class);
    Cliente toEntity(ClienteVO clienteVO);
    ClienteVO toClienteVO(Cliente cliente);
}