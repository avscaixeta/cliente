package com.builders.cliente.exception;

import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {
    private String code;

    private Object[] params;

    public BusinessException(String message, Object... params) {
        this(null, message, params);
    }

    public BusinessException(Throwable cause, String code, Object... params) {
        super(code, cause);
        this.code = code;
        this.params = params;
    }

}

