package com.builders.cliente.controller;

import com.builders.cliente.service.ClienteService;
import com.builders.cliente.utils.Constants;
import com.builders.cliente.vo.ClienteVO;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/v1/clientes", produces = "application/json")
@Api(value = "Serviço responsável pelos Clientes do sistema.", tags={"ClienteController"})
public class ClienteController {

    private final ClienteService clienteService;

    @GetMapping
    public ResponseEntity<Page<ClienteVO>> findAllClients(@RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo,
                                                          @RequestParam(value = "pageSize", required = false, defaultValue = "5") int pageSize,
                                                          @RequestParam(value = "nome", required = false, defaultValue = "") String nome,
                                                          @RequestParam(value = "cpf", required = false, defaultValue = "") String cpf) {
        return new ResponseEntity<>(clienteService.findPaginatedClients(nome, cpf, pageNo, pageSize), HttpStatus.OK);
    }

    @PostMapping(consumes = Constants.APPLICATION_JSON)
    public ResponseEntity<String> saveClient(@Valid @NotNull @RequestBody ClienteVO cliente) {
        clienteService.saveClient(cliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(Constants.CLIENTE_CRIADO_COM_SUCESSO);
    }

    @PatchMapping(value = "{cpf}", consumes = Constants.APPLICATION_JSON)
    public ResponseEntity<String> changeClientName(@NotNull @PathVariable String cpf,
                                                   @RequestBody @Valid @NotNull ClienteVO cliente) {
        clienteService.updateClientName(cpf, cliente.getNome());
        return ok(Constants.NOME_CLIENTE_ALTERADO_COM_SUCESSO);
    }

    @PutMapping(consumes = Constants.APPLICATION_JSON)
    public ResponseEntity<String> updateClient(@Valid @NotNull @RequestBody ClienteVO cliente) {
        clienteService.updateClient(cliente);
        return ok(Constants.CLIENTE_ALTERADO_COM_SUCESSO);
    }

    @DeleteMapping(value = "{cpf}")
    public ResponseEntity<String> deleteClient(@NotNull @PathVariable String cpf) {
        clienteService.deleteClient(cpf);
        return ok(Constants.CLIENTE_DELETADO_COM_SUCESSO);
    }

}
