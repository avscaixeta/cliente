package com.builders.cliente.vo;

import com.builders.cliente.utils.CalendarDateDeserializer;
import com.builders.cliente.utils.CalendarSerializer;
import com.builders.cliente.utils.Utils;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Calendar;

@Getter
@Setter
public class ClienteVO {
    private String cpf;

    private String nome;

    @JsonDeserialize(using = CalendarDateDeserializer.class)
    @JsonSerialize(using = CalendarSerializer.class)
    private Calendar dataNascimento;

    public Integer getIdade() {
        return Utils.ageCalc(getDataNascimento());
    }

}
