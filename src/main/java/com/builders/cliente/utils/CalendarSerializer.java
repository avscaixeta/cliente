package com.builders.cliente.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


public class CalendarSerializer extends JsonSerializer<Calendar> {

    @Override
    public void serialize(Calendar value, JsonGenerator jgen, SerializerProvider sp) throws IOException {
        if (value != null)
            jgen.writeString(new SimpleDateFormat(Constants.DD_MM_YYYY).format(value.getTime()));
    }
}

