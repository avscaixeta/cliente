package com.builders.cliente.utils;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;

public class Utils {

    public static int ageCalc(Calendar dataNascimento) {
        LocalDate dataNascimentoLocalDate = LocalDate.ofInstant(dataNascimento.toInstant(), ZoneId.systemDefault());
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(dataNascimentoLocalDate.getYear(),
                dataNascimentoLocalDate.getMonth(), dataNascimentoLocalDate.getDayOfMonth());
        return Period.between(birthday, today).getYears();
    }
}
