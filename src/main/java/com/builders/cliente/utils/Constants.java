package com.builders.cliente.utils;

public interface Constants {

    String CLIENTE_CRIADO_COM_SUCESSO = "Cliente criado com sucesso";
    String NOME_CLIENTE_ALTERADO_COM_SUCESSO = "Nome do cliente alterado com sucesso";
    String CLIENTE_ALTERADO_COM_SUCESSO = "Cliente alterado com sucesso";
    String CLIENTE_DELETADO_COM_SUCESSO = "Cliente deletado com sucesso";

    String APPLICATION_JSON = "application/json";

    String NENHUM_CLIENTE_ENCONTRADO_COM_ESSE_CPF = "Nenhum cliente encontrado com esse CPF.";
    String CAMPOS_OBRIGATORIOS_N_INFORMADOS = "Campo(s) obrigatório(s) não informado(s).";

    String DD_MM_YYYY = "dd/MM/yyyy";
    String FORMATO_DATA_FORA_PADROES = "Formato de data fora dos padrões: ";
}
