package com.builders.cliente.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class CalendarDateDeserializer extends JsonDeserializer<Calendar> {

    @Override
    public Calendar deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext)
            throws IOException {
        return fromStringDateToCalendar(jsonparser.getText());
    }

    private Calendar fromStringDateToCalendar(String dateString) {
        Calendar cal = null;
        DateFormat data = new SimpleDateFormat(Constants.DD_MM_YYYY);;

        if (dateString != null && dateString.length() > 0) {
            try {
                cal = Calendar.getInstance();
                int size = dateString.length();
                if (size == Constants.DD_MM_YYYY.length())
                    cal.setTime(data.parse(dateString));
                else
                    throw new RuntimeException(Constants.FORMATO_DATA_FORA_PADROES + dateString);
            } catch (ParseException e1) {
                cal = null;
            }
        }

        return cal;
    }
}
