# README #
# PLATFORM Builders - Desafio Java Sr
Repositório do desafio do processo seletivo para o cargo de Desenvolvedor Java Sr da Platform Builders

## Arquitetura

```
- Java 11
- Spring Boot
- Maven (gerenciamento das dependências)
- Lombok (redução de boilerplate)
- Swagger (documentacão dos endpoints)
- Flyway (versionamento de scripts de banco)
- Teste unitário com Junit/Mockito
- PostgreSQL
```

## Instruções para execução dos projetos


#### Pré-Requisitos
```
Docker
Docker Compose
Java 11
Maven
```

#### Passo a passo

##### Realizar o clone/download do projeto
```
Entrar na pasta do projeto e rodar os comandos do maven para montar as classes e baixar as dependências
- mvn clean install
```


#### Entrar na pasta 'db' e executar o comando para baixar a imagem e subir o container docker
```
Nessa pasta existe um docker file (docker-compose.yml) com as instruções para o docker baixar uma imagem do Postgres e montar o container.
Existe também um arquivo sql para o docker subir uma instância de uma base de dados.
- docker-compose up -d
```

#### Executar o comando do flyway para criar a estrutura do banco de dados.
```
mvn flyway:migrate
```

#### E finalmente rodar o projeto backend (por padrão irá rodar na porta 8080)
```
mvn spring-boot:run
```

Obs: O arquivo contendo a collection do postman para teste do projeto está na raiz com o nome de "CLIENTE.postman_collection.json"